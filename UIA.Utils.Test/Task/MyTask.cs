﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace UIA.Utils
{
    public class MyTask : ITask
    {
        public String Name { get; set; }

        public int Idle { get; set; }

        public bool Execute()
        {
            Console.WriteLine(Name + ":" + Idle);
            Thread.Sleep(Idle);
            return true;
        }

        public override String ToString()
        {
            return Name;
        }
    }
}
