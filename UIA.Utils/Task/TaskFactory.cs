﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace UIA.Utils
{
    public delegate void ProgressEvent(ITask current, bool success, int part, int total);

    public class TaskFactory
    {
        public event ProgressEvent ReportProgress;

        public BlockingCollection<ITask> Tasks { get; private set; }

        public Dictionary<ITask, bool> Result { get; private set; }

        public TaskFactory()
        {
            Tasks = new BlockingCollection<ITask>();
            Result = new Dictionary<ITask, bool>();
        }

        public void Execute()
        {
            Dictionary<ITask, bool> result = new Dictionary<ITask, bool>();
            int total = Tasks.Count;
            int finished = 0;
            Parallel.ForEach(Tasks, task =>
            {
                bool val = false;
                try {
                    if (task != null)
                    {
                        val = task.Execute();
                        Result[task] = val;
                    }
                }
                catch
                {
                }
                finally
                {
                    int _finish = 0;
                    lock (this)
                    {
                        finished++;
                        _finish = finished;
                    }
                    if (ReportProgress != null)
                    {
                        try
                        {
                            ReportProgress(task, val, _finish, total);
                        }
                        catch
                        {

                        }
                    }
                }
            });
        }
    }
}
