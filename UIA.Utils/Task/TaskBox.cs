﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UIA.Utils
{
    public class TaskBox<T> : ITask
    {
        public IList<T> Values { get; private set; }

        public ITaskExecutor<T> Executor { get; set; }

        public TaskBox()
        {
            Values = new List<T>();
        }

        public bool Execute()
        {
            bool result = true;
            foreach(T value in Values)
            {
                result = Executor.Accept(value) && result;
            }
            return result;
        }


    }
}
