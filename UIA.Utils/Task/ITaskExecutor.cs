﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UIA.Utils
{
    public interface ITaskExecutor<T>
    {
        bool Accept(T value);
    }
}
