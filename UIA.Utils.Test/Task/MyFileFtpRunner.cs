﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace UIA.Utils
{
    public class MyFileFtpRunner : ITaskExecutor<MyFile>
    {
        public bool Accept(MyFile value)
        {
            Thread.Sleep(value.Size);
            Console.WriteLine(value + " is sent");
            return true;
        }
    }
}
