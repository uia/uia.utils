﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UIA.Utils
{
    public class MyFile
    {
        public String Name { get; set; }

        public int Size { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
