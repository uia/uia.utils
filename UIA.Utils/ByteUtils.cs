﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UIA.Utils
{
    public static class ByteUtils
    {
        /// <summary>
        /// Convert data to hex style string.
        /// </summary>
        /// <param name="data">Data</param>
        /// <param name="split">Split character</param>
        /// <returns>Hex string</returns>
        public static string ToHexString(this byte[] data, char split = '-')
        {
            if (data == null || data.Length == 0)
            {
                return "";
            }

            String result = "";
            for (int i = 0; i < data.Length - 1; i++)
            {
                result += (data[i].ToString("X2") + split);
            }
            return result + data[data.Length - 1].ToString("X2");
        }

        /// <summary>
        /// Convert data to string with ASCII encoding.
        /// </summary>
        /// <param name="data">Data</param>
        /// <returns>Result</returns>
        public static string ToAsciiString(this byte[] data)
        {
            return Encoding.ASCII.GetString(data);
        }

        /// <summary>
        /// Convert data to string with specific encoding.
        /// </summary>
        /// <param name="data">Data</param>
        /// <param name="encoding">Encoding</param>
        /// <returns>Result</returns>
        public static string ToString(this byte[] data, Encoding encoding)
        {
            return encoding.GetString(data);
        }
    }
}
