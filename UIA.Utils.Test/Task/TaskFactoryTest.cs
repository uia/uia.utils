﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UIA.Utils
{
    [NUnit.Framework.TestFixture]
    public class TaskFactoryTest
    {
        [NUnit.Framework.Test]
        public void TestTask()
        {
            TaskFactory factory = new TaskFactory();
            factory.Tasks.Add(new MyTask() { Name = "Case1", Idle = 2000 });
            factory.Tasks.Add(new MyTask() { Name = "Case2", Idle = 2000 });
            factory.Tasks.Add(new MyTask() { Name = "Case3", Idle = 2000 });
            factory.Tasks.Add(new MyTask() { Name = "Case4", Idle = 2000 });
            factory.Tasks.Add(new MyTask() { Name = "Case5", Idle = 2000 });
            factory.Tasks.Add(new MyTask() { Name = "Case6", Idle = 2000 });
            factory.Tasks.Add(new MyTask() { Name = "Case7", Idle = 2000 });
            factory.Tasks.Add(new MyTask() { Name = "Case8", Idle = 2000 });
            factory.Tasks.Add(new MyTask() { Name = "Case9", Idle = 2000 });
            factory.Tasks.Add(new MyTask() { Name = "Case10", Idle = 2000 });
            factory.Tasks.Add(new MyTask() { Name = "Case11", Idle = 2000 });
            factory.Tasks.Add(new MyTask() { Name = "Case12", Idle = 2000 });
            factory.Tasks.Add(new MyTask() { Name = "Case13", Idle = 2000 });
            factory.Tasks.Add(new MyTask() { Name = "Case14", Idle = 2000 });
            factory.Tasks.Add(new MyTask() { Name = "Case15", Idle = 2000 });
            factory.Tasks.Add(new MyTask() { Name = "Case16", Idle = 2000 });

            DateTime t = DateTime.Now;
            factory.Execute();
            TimeSpan ts = DateTime.Now - t;
            Console.WriteLine("Spend:" + ts.TotalMilliseconds);
        }

        [NUnit.Framework.Test]
        public void TestTaskBox()
        {
            TaskBox<MyFile> ftp1 = new TaskBox<MyFile>();
            ftp1.Executor = new MyFileFtpRunner();
            ftp1.Values.Add(new MyFile() { Name = "FTP1-FILE1", Size = 2000 });
            ftp1.Values.Add(new MyFile() { Name = "FTP1-FILE2", Size = 3000 });
            ftp1.Values.Add(new MyFile() { Name = "FTP1-FILE3", Size = 4000 });

            TaskBox<MyFile> ftp2 = new TaskBox<MyFile>();
            ftp2.Executor = new MyFileFtpRunner();
            ftp2.Values.Add(new MyFile() { Name = "FTP2-FILE1", Size = 2000 });
            ftp2.Values.Add(new MyFile() { Name = "FTP2-FILE2", Size = 3000 });
            ftp2.Values.Add(new MyFile() { Name = "FTP2-FILE3", Size = 4000 });
            ftp2.Values.Add(new MyFile() { Name = "FTP2-FILE4", Size = 5000 });
            ftp2.Values.Add(new MyFile() { Name = "FTP2-FILE5", Size = 2000 });

            TaskBox<MyFile> ftp3 = new TaskBox<MyFile>();
            ftp3.Executor = new MyFileFtpRunner();
            ftp3.Values.Add(new MyFile() { Name = "FTP3-FILE1", Size = 1000 });
            ftp3.Values.Add(new MyFile() { Name = "FTP3-FILE2", Size = 2000 });
            ftp3.Values.Add(new MyFile() { Name = "FTP3-FILE3", Size = 3000 });
            ftp3.Values.Add(new MyFile() { Name = "FTP3-FILE4", Size = 4000 });

            TaskFactory factory = new TaskFactory();
            factory.Tasks.Add(ftp1);
            factory.Tasks.Add(ftp2);
            factory.Tasks.Add(ftp3);

            DateTime t = DateTime.Now;
            factory.Execute();
            TimeSpan ts = DateTime.Now - t;
            Console.WriteLine("Spend:" + ts.TotalMilliseconds);
        }
    }
}
