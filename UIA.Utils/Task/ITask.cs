﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UIA.Utils
{
    public interface ITask
    {
        bool Execute();
    }
}
