﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UIA.Utils
{
    public static class StringUtils
    {
        /// <summary>
        /// Convert string to bytes with ASCII encoding.
        /// </summary>
        /// <param name="data">Content</param>
        /// <returns>Byte array.</returns>
        public static byte[] ToAsciiBytes(this string data)
        {
            return Encoding.ASCII.GetBytes(data);
        }

        /// <summary>
        /// Convert string to hex formatted string with ASCII encoding.
        /// </summary>
        /// <param name="data">Content.</param>
        /// <param name="split">Character bteween hex string.</param>
        /// <returns>Hex string.</returns>
        public static string Hex(this string data, char split = '-')
        {
            return data.ToAsciiBytes().ToHexString(split);
        }

        /// <summary>
        /// Convert string with specific encoding.
        /// </summary>
        /// <param name="data">Content</param>
        /// <param name="encoding">Encoding of content.</param>
        /// <returns>Byte array.</returns>
        public static byte[] ToBytes(this string data, Encoding encoding)
        {
            return encoding.GetBytes(data);
        }

        /// <summary>
        /// Convert hex formatted string to bytes. 
        /// Ex. "ff-06-e5-5a-a6" >> new byte[] { 0xff, 0x06, 0xe5, 0x5a, 0xa6 }.
        /// </summary>
        /// <param name="data">Hex formatted string.</param>
        /// <param name="split">Character between hex.</param>
        /// <returns>Bytes array.</returns>
        public static byte[] ToHexBytes(this string data, char split = '-')
        {
            string[] hexValues = data.Split(split);
            byte[] result = new byte[hexValues.Length];
            for (int i = 0; i < hexValues.Length; i++)
            {
                result[i] = (byte)Convert.ToInt32(hexValues[i], 16);
            }
            return result;
        }
    }
}
